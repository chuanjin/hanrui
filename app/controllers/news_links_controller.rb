class NewsLinksController < ApplicationController
  before_action :authenticate_user!, except: [:index]

  def index
    @news_links = NewsLink.page(params[:page]).order(created_at: :desc)
  end

  def new
    @news_link = NewsLink.new
  end

  def create
    @news_link = NewsLink.create(news_link_params)
    if @news_link.save
      redirect_to news_links_path
    else
      render 'new'
    end
  end

  #def show
    #@news_link = NewsLink.find(params[:id])
    #respond_to do |format|
      #format.html
    #end
  #end

  def edit
    @news_link = NewsLink.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def update
    @news_link = NewsLink.find(params[:id])

    if @news_link.update(news_link_params)
      redirect_to news_links_path
    else
      render 'edit'
    end
  end


  def destroy
    @news_link = NewsLink.find(params[:id])
    @news_link.destroy

    redirect_to news_links_path
  end

  private
  # Using a private method to encapsulate the permissible parameters
  # is just a good pattern since you'll be able to reuse the same
  # permit list between create and update. Also, you can specialize
  # this method with per-user checking of permissible attributes.
  def news_link_params
    params.require(:news_link).permit(:title, :link)
  end

end
