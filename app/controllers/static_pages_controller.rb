class StaticPagesController < ApplicationController
  def home
    @news_links = NewsLink.order(created_at: :desc).limit(10)
  end

  def about
  end

  def activities
  end

  def services
    @tables = Dir.glob("app/assets/files/*.docx")
  end

  def faq
  end

  def disclaimer
  end

  def privacy
  end

end
