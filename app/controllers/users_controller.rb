class UsersController < ApplicationController
  include UsersHelper

  before_action :authenticate_user!
  before_action :has_permissioin , except: :index

  def index
    unless current_user.admin?
      flash[:notice] = "Must be admin!"
      redirect_to current_user
    end
    @users = User.paginate(page: params[:page]).order(created_at: :desc)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to user_path
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
    @user.destroy
    flash[:notice] = "User '#{@user.email}' deleted."
    redirect_to users_path
  end


  private
  def user_params
    params.require(:user).permit(:id, :full_name, :company)
  end

  def has_permissioin
    @user = User.find(params[:id])
    unless (@user and @user == current_user) or current_user.admin?
      flash[:notice] = "Must be admin!"
      redirect_to current_user
    end
  end


end
