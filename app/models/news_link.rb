class NewsLink < ActiveRecord::Base
  validates_presence_of :title, :message => "Title can't be blank!"
  validates_uniqueness_of :title, :message => "Title already exists!"
  validates_presence_of :link, :message => "Link can't be blank!"
  validate :valide_url

  self.per_page = 10

  def valide_url
    unless link =~ /\A#{URI::regexp(['http', 'https'])}\z/
      errors.add(:link, "Invalid URL!")
    end
  end
end
