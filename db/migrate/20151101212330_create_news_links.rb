class CreateNewsLinks < ActiveRecord::Migration
  def change
    create_table :news_links do |t|
      t.string :title
      t.string :link

      t.timestamps null: false
    end
  end
end
