# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(email: 'admin@hansebridge.org', password: 'password', confirmed_at: "2015-12-23 14:34:34", admin: true)

49.times do |n|
  email = "example-#{n+1}@hansebridge.org"
  password = "password"
  User.create!(
    email: email,
    password: password,
    confirmed_at: Time.now
  )
end


50.times do |n|
  title = "example-#{n+1} news added by users"
  link = "http://hansebridge.org"
  NewsLink.create!(
    title: title,
    link: link
  )
end
