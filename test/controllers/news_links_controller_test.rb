require 'test_helper'

class NewsLinksControllerTest < ActionController::TestCase

  def setup
    @news = news_links(:news1)
  end

  test "should get news index page" do
    get :index
    assert_response :success

    assert_select 'div.pagination'
    first_page_of_news = NewsLink.paginate(page: 1)
    first_page_of_news.each do |news|
      assert_select 'a[href=?]', edit_news_link_path(news), text: "Edit"
      assert_select 'a[href=?]', news_link_path(news), text: "Delete"
    end
  end

  test "should get new page" do
    @user = users(:user1)
    sign_in @user
    get :new
    assert_response :success
  end

  test "should not get new page when not login" do
    get :new
    assert_response 302
  end

  test "should create news link" do
    @user = users(:user1)
    sign_in @user
    assert_difference('NewsLink.count', 1) do
      post :create, news_link: {title:"Example Title", link: "http://test.com"}
    end
  end

  test "should render new when create invalid" do
    @user = users(:user1)
    sign_in @user
    post :create, news_link: {title:"Example Title", link: "test"}
    assert_template :new
  end

  test "should not create when not login" do
    post :create, news_link: {title:"Example Title", link: "http://test.com"}
    assert_response 302
  end

  test "should get edit page" do
    @user = users(:user1)
    sign_in @user
    get :edit, id: @news.id
    assert_response :success
  end

  test "should not get edit page when not login" do
    get :edit, id: @news.id
    assert_response 302
  end

  test "should update news link" do
    @user = users(:user1)
    sign_in @user
    patch :update, id: @news.id, news_link: {title:"Example Title1", link: "http://test.com"}
    assert_redirected_to news_links_path
  end

  test "should render edit when update invalid" do
    @user = users(:user1)
    sign_in @user
    patch :update, id: @news.id, news_link: {title:"Example Title1", link: "test"}
    assert_template :edit
  end

  test "should not update when not login" do
    patch :update, id: @news.id, news_link: {title:"Example Title1", link: "http://test.com"}
    assert_response 302
  end

  test "should destroy news link" do
    @user = users(:user1)
    sign_in @user
    assert_difference('NewsLink.count', -1) do
      delete :destroy, id: @news.id
    end
  end

  test "should not destroy when not login" do
    delete :destroy, id: @news.id
    assert_response 302
  end

end
