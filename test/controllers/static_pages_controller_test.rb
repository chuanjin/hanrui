require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  def setup
    @base_title = "HanSEbridge"
  end

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "#{@base_title} | Home"
  end

  test "should get activities" do
    get :activities
    assert_response :success
    assert_select "title", "#{@base_title} | Activities"
  end

  test "should get services" do
    get :services
    assert_response :success
    assert_select "title", "#{@base_title} | Services"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "#{@base_title} | About"
  end

  test "should get faq" do
    get :faq
    assert_response :success
    assert_select "title", "#{@base_title} | FAQ"
  end

  test "should get privacy" do
    get :privacy
    assert_response :success
    assert_select "title", "#{@base_title} | Privacy"
  end

  test "should get disclaimer" do
    get :disclaimer
    assert_response :success
    assert_select "title", "#{@base_title} | Disclaimer"
  end


  test "layout links" do
    get :home
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", services_path
    #assert_select "a[href=?]", activities_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", faq_path
    assert_select "a[href=?]", privacy_path
    assert_select "a[href=?]", disclaimer_path
    assert_select "a[href=?]", new_user_session_path
    assert_select "a[href=?]", new_user_registration_path
  end

end
