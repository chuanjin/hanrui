require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  test "Admin should get index page" do
    @admin = users(:admin)
    sign_in @admin
    get :index
    assert_response :success
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: "Show"
      assert_select 'a[href=?]', edit_user_path(user), text: "Edit"
      assert_select 'a[href=?]', user_path(user), text: "Delete"
    end
  end

  test "Non admin should not get index page" do
    get :index
    assert_response 302
    @user = users(:user1)
    sign_in @user
    get :index
    assert_response 302
  end

  test "Admin should get any show page" do
    @admin = users(:admin)
    @user = users(:user1)
    sign_in @admin
    get :show, id: @admin.id
    assert_response :success
    get :show, id: @user.id
    assert_response :success
  end

  test "User should only get himself show page" do
    @user = users(:user1)
    @user2 = users(:user2)
    sign_in @user
    get :show, id: @user.id
    assert_response :success
    get :show, id: @user2.id
    assert_response 302
  end

  test "Admin should get any edit page" do
    @admin = users(:admin)
    @user = users(:user1)
    sign_in @admin
    get :edit, id: @admin.id
    assert_response :success
    get :edit, id: @user.id
    assert_response :success
  end

  test "User should only get himself edit page" do
    @user = users(:user1)
    @user2 = users(:user2)
    sign_in @user
    get :edit, id: @user.id
    assert_response :success
    get :edit, id: @user2.id
    assert_response 302
  end

  test "Admin should update any" do
    @admin = users(:admin)
    @user = users(:user1)
    sign_in @admin
    put :update, id: @user.id, user:{last_name:"Potter", first_name:"Harry"}
    assert_redirected_to @user
    put :update, id: @admin.id, user:{last_name:"Potter", first_name:"Harry"}
    assert_redirected_to @admin
  end

  test "User should only update himself" do
    @user = users(:user1)
    @user2 = users(:user2)
    sign_in @user
    put :update, id: @user2.id, user:{last_name:"Potter", first_name:"Harry"}
    assert_response 302
    put :update, id: @user.id, user:{last_name:"Potter", first_name:"Harry"}
    assert_redirected_to @user
  end


  test "Admin should destroy any" do
    @admin = users(:admin)
    @user = users(:user1)
    sign_in @admin
    assert_difference('User.count', -1) do
      delete :destroy, id: @user.id
    end
    assert_redirected_to users_path
    assert_difference('User.count', -1) do
      delete :destroy, id: @admin.id
    end
  end

  test "User should only destroy himself" do
    @user = users(:user1)
    @user2 = users(:user2)
    sign_in @user
    delete :destroy, id: @user2.id
    assert_response 302
    assert_difference('User.count', -1) do
      delete :destroy, id: @user.id
    end
  end

end
