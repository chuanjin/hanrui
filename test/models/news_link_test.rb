require 'test_helper'

class NewsLinkTest < ActiveSupport::TestCase

  def setup
    @news_link = NewsLink.new(title: "Example News", link: "http://dummy.com")
  end

  test "should be valid" do
    assert @news_link.valid?
  end


  test "title should be present" do
    @news_link.title = "     "
    assert_not @news_link.valid?
  end

  test "link should be present" do
    @news_link.link = "     "
    assert_not @news_link.valid?
  end

  test "title should be unique" do
    @news_link1 = NewsLink.new(title: "Example News", link: "http://dummy.com")
    @news_link2 = NewsLink.new(title: "Example News", link: "http://dummy.com")
    @news_link1.save
    assert_not @news_link2.save
  end

  test "URL should be valid" do
    @news_link.link = "something"
    assert_not @news_link.valid?
  end


end
